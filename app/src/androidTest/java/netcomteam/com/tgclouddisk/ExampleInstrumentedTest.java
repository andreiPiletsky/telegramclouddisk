package netcomteam.com.tgclouddisk;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;
import static org.hamcrest.core.Is.is;

import netcomteam.com.tgclouddisk.handlers.ApiHelper;
import netcomteam.com.tgclouddisk.ui.fragments.BaseFragment;
import netcomteam.com.tgclouddisk.user.User;
import netcomteam.com.tgclouddisk.user.UserInformation;
/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    Context context;
    BaseFragment fragment = new BaseFragment();

    @Test
    public void useAppContext() throws Exception {
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("netcomteam.com.tgclouddisk", appContext.getPackageName());
    }

//    @Test
//    public void test1_userStatus() throws Exception {
//        UserInformation userInformation = new UserInformation();
//        User user = new User(context);
//        user.getInformationAboutUser();
//        Thread.sleep(1500);
//        Log.w("test____userStatus", String.valueOf(userInformation.getId()));
//        assertNotEquals(new UserInformation().getId(), 0);
//    }
//
//    @Test
//    public void test2_uploadFiles() throws Exception {
//        URL imageurl = new URL("http://rusargument.ru/data/photo/061616_034814515892.png");
//        Bitmap _bitmapScaled = BitmapFactory.decodeStream(imageurl.openConnection().getInputStream());
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        _bitmapScaled.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
//        File f = new File(Environment.getExternalStorageDirectory()
//                + File.separator + "test.jpg");
//        f.createNewFile();
//        FileOutputStream fo = new FileOutputStream(f);
//        fo.write(bytes.toByteArray());
//        fo.close();
//        fragment.upLoadPhoto(f.getPath(), "/");
//        Log.w("fragmetndata", String.valueOf(fragment.data.size()));
//    }
//
//    @Test
//    public void test3_collectionFiles() throws Exception {
//        fragment.loadData();
//        Thread.sleep(1500);
//        Log.w("test____colection", String.valueOf(fragment.data.size()));
//        assertThat(fragment.data.isEmpty(), is(false));
//        String returnCollection = fragment.data.getClass().getName() + "<" + fragment.data.get(0).getClass().getName() + ">";
//        assertEquals("java.util.ArrayList<netcomteam.com.tgclouddisk.pojo.FileModel>", returnCollection);
//    }
//
//    @Test
//    public void test4_deleteFiles() throws Exception {
//        fragment.loadData();
//        Thread.sleep(1500);
//        ApiHelper apiHelper = new ApiHelper();
//        apiHelper.deleteFile(fragment.countFolder, fragment.data);
//    }
}
