package netcomteam.com.tgclouddisk;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;

import org.drinkless.td.libcore.telegram.TG;

import java.io.File;


public class App extends Application {
    SharedPreferences prefs;
    String lang;

    @Override
    public void onCreate() {
        super.onCreate();
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        lang =prefs.getString("lang", "ru");

        prefs.edit().putString("lang", "ru").commit();
        //lang = "en"or whatever the saved language before restarting the device
        //if there is no saved string with this key "lang" lang variable will be
//        equal "en"
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            String path = Environment.getExternalStorageDirectory() + File.separator + "Android" + File.separator + "data" + File.separator + getPackageName();
            TG.setDir(path);
            TG.setFilesDir(Environment.getExternalStorageDirectory()+ File.separator + getResources().getString(R.string.app_name));
        }
        Log.w("actttttttttttt","app");
    }
}
