package netcomteam.com.tgclouddisk;

/**
 * Created by andreika on 11.10.17.
 */

public class Const {
    public final static int COUNT_LOAD_MESSAGES = 50;
    public final static int PHOTO_TYPE=1;
    public final static int AUDIO_TYPE=2;
    public final static int VIDEO_TYPE=3;
    public final static int FILE_TYPE=4;

}
