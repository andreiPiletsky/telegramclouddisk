package netcomteam.com.tgclouddisk;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;

import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;
/**
 * Created by andreika on 25.12.17.
 */
public class LocaleLanguage {

    Context context;

    public LocaleLanguage(Context context) {
        this.context = context;
    }

    public static final String FILE_NAME = "file_lang"; // preference file name
    public static final String KEY_LANG = "key_lang"; // preference key

    public void saveLanguage(String lang) {
        // we can use this method to save language
        SharedPreferences preferences = context.getSharedPreferences(FILE_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_LANG, lang);
        editor.apply();
    }

    public void loadLanguage() {
        // we can use this method to load language,
        // this method should be called before setContentView() method of the onCreate method
        Locale locale = new Locale(getLangCode());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }

    public String getLangCode() {
        SharedPreferences preferences = context.getSharedPreferences(FILE_NAME, MODE_PRIVATE);
        String langCode = preferences.getString(KEY_LANG, "ru");
        // save english 'en' as the default language
        return langCode;
    }
}
