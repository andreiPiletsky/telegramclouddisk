package netcomteam.com.tgclouddisk.pojo;
import org.drinkless.td.libcore.telegram.TdApi;
/**
 * Created by andreika on 26.09.17.
 */
public class ChatModel {

    private static long id;
    private static TdApi.Message topMessage;
    private static int unreadCount;
    private static int lastReadInboxMessageId;
    private static int lastReadOutboxMessageId;

    public ChatModel() {
    }

    public static long getId() {
        return id;
    }

    public static TdApi.Message getTopMessage() {
        return topMessage;
    }

    public static int getUnreadCount() {
        return unreadCount;
    }

    public static int getLastReadInboxMessageId() {
        return lastReadInboxMessageId;
    }

    public static int getLastReadOutboxMessageId() {
        return lastReadOutboxMessageId;
    }

    public static void setId(long id) {
        ChatModel.id = id;
    }

    public static void setTopMessage(TdApi.Message topMessage) {
        ChatModel.topMessage = topMessage;
    }

    public static void setUnreadCount(int unreadCount) {
        ChatModel.unreadCount = unreadCount;
    }

    public static void setLastReadInboxMessageId(int lastReadInboxMessageId) {
        ChatModel.lastReadInboxMessageId = lastReadInboxMessageId;
    }

    public static void setLastReadOutboxMessageId(int lastReadOutboxMessageId) {
        ChatModel.lastReadOutboxMessageId = lastReadOutboxMessageId;
    }
}
