package netcomteam.com.tgclouddisk.pojo;
/**
 * Created by andreika on 03.10.17.
 */
public class CountryModel {

    private String name;
    private String code;

    public CountryModel(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}