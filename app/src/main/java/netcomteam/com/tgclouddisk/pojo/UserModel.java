package netcomteam.com.tgclouddisk.pojo;
import org.drinkless.td.libcore.telegram.TdApi;
/**
 * Created by andreika on 26.09.17.
 */

public class UserModel {
    private static int id;
    private static String firstName;
    private static String lastName;
    private static String username;
    private static String phone_number;
    private static TdApi.UserStatus status;
//    private static TdApi.ProfilePhoto profilePhoto;

//    public static void setProfilePhoto(TdApi.ProfilePhoto profilePhoto) {
//        UserModel.profilePhoto = profilePhoto;
//    }

//    public static TdApi.ProfilePhoto getProfilePhoto() {
//        return profilePhoto;
//    }

    public UserModel() {

    }

    public static int getId() {
        return id;
    }

    public static String getFirstName() {
        return firstName;
    }

    public static String getLastName() {
        return lastName;
    }

    public static String getUsername() {
        return username;
    }



    public static TdApi.UserStatus getStatus() {
        return status;
    }


    public static String getPhone_number() {
        return phone_number;
    }

    public static void setPhone_number(String phone_number) {
        UserModel.phone_number = phone_number;
    }

    public static void setId(int id) {
        UserModel.id = id;
    }

    public static void setFirstName(String firstName) {
        UserModel.firstName = firstName;
    }

    public static void setLastName(String lastName) {
        UserModel.lastName = lastName;
    }

    public static void setUsername(String username) {
        UserModel.username = username;
    }



    public static void setStatus(TdApi.UserStatus status) {
        UserModel.status = status;
    }



}
