package netcomteam.com.tgclouddisk.ui.activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import netcomteam.com.tgclouddisk.LocaleLanguage;
import netcomteam.com.tgclouddisk.R;
public class ActivationActivity extends AppCompatActivity {

    private static final int ACTIVATION_CODE_LENGTH = 5;
    @BindView(R.id.activation_information)
    TextView activationInformation;
    @BindView(R.id.input_code)
    EditText code;

    @OnTextChanged(R.id.input_code)
    void onActivationCodeChanged(CharSequence text) {
        if (text.length() == ACTIVATION_CODE_LENGTH) {
            doActivate();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocaleLanguage localeLanguage = new LocaleLanguage(ActivationActivity.this);
        localeLanguage.loadLanguage();
        setContentView(R.layout.activation_layout);
        ButterKnife.bind(this);
        Log.w("oncreate", "sdf");
    }

    private void doActivate() {
        Log.w("activation", "sdf");
        TdApi.CheckAuthCode authSetCode = new TdApi.CheckAuthCode(code.getText().toString(), "", "");
        TG.getClientInstance().send(authSetCode, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object.getConstructor() == TdApi.AuthStateOk.CONSTRUCTOR) {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Incorrect code", Toast.LENGTH_SHORT);
                }
            }
        });
    }
}