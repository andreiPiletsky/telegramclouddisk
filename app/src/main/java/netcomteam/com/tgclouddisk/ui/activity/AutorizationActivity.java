package netcomteam.com.tgclouddisk.ui.activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import netcomteam.com.tgclouddisk.LocaleLanguage;
import netcomteam.com.tgclouddisk.R;
import netcomteam.com.tgclouddisk.user.User;
public class AutorizationActivity extends AppCompatActivity {

    @BindView(R.id.country_code)
    TextView country_code;
    @BindView(R.id.input_phoneNumber)
    EditText input_phoneNumber;
    @BindView(R.id.country_name)
    TextView country_name;
    String phoneNumber;
    User user;

    @OnClick(R.id.country_name)
    void chooseCountryName() {
        Intent intent = new Intent(AutorizationActivity.this, CountriesListActivity.class);
        startActivityForResult(intent, RESULT_FIRST_USER);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            country_name.setText(data.getStringExtra("name"));
            country_code.setText(data.getStringExtra("code"));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = new User(AutorizationActivity.this);
        LocaleLanguage localeLanguage = new LocaleLanguage(AutorizationActivity.this);
        localeLanguage.loadLanguage();
        setContentView(R.layout.registration_layout);
        ButterKnife.bind(this);
        Log.w("reg", "sdf");
    }

    @OnClick(R.id.press_check_phoneNumber)
    public void submitNumber() {
        phoneNumber = input_phoneNumber.getText().toString();
        if (phoneNumber.length() == 10) {
            phoneNumber = country_code.getText().toString() + input_phoneNumber.getText().toString();
            user.log_in(phoneNumber,country_code,input_phoneNumber);
        } else {
            reportWrongNumberError();
        }
    }

    private void reportWrongNumberError() {
        Toast.makeText(this, "Invalid phone number", Toast.LENGTH_SHORT).show();
    }
}