package netcomteam.com.tgclouddisk.ui.activity;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import netcomteam.com.tgclouddisk.R;
import netcomteam.com.tgclouddisk.ui.adapters.GalleryAdapter;
import netcomteam.com.tgclouddisk.ui.fragments.GalleryFragment;
/**
 * Created by andreika on 28.09.17.
 */
public class FullScreenImageActivity extends AppCompatActivity {

    @BindView(R.id.imageViewFullScreen)
    ImageView imageView;
    private String url;
    int file_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_full_image);
        ButterKnife.bind(this);
        url = getIntent().getStringExtra("url");
        file_id = getIntent().getIntExtra("file_id", 0);
        Log.w("file", String.valueOf(file_id));
        Glide.with(this).load(new File(url)).thumbnail(0.1f).into(imageView);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}