package netcomteam.com.tgclouddisk.ui.activity;//package netcomteam.com.newtgdisk.ui.activity;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import netcomteam.com.tgclouddisk.LocaleLanguage;
import netcomteam.com.tgclouddisk.R;
import netcomteam.com.tgclouddisk.ui.adapters.CountryAdapter;
public class LanguagesListActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @OnClick(R.id.toolbar)
    void cancelResult() {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_languages);
        ButterKnife.bind(this);
        toolbar.setTitle(getResources().getString(R.string.language));
        toolbar.setTitleTextColor(Color.WHITE);
        final ListView listView = (ListView) findViewById(R.id.languages_list);
        final String[] lang = new String[]{
                "Русский", "English"
        };
// используем адаптер данных
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, lang);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String lang;
                if (position == 0) {
                    lang = "ru";
                } else {
                    lang = "en";
                }
                LocaleLanguage localeLanguage = new LocaleLanguage(LanguagesListActivity.this);
                localeLanguage.saveLanguage(lang);
                Intent intent = new Intent();
//                intent.putExtra("lang", (String) locale.getLanguage());
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
    }
}
