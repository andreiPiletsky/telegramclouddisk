package netcomteam.com.tgclouddisk.ui.activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wang.avi.AVLoadingIndicatorView;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import netcomteam.com.tgclouddisk.LocaleLanguage;
import netcomteam.com.tgclouddisk.R;
import netcomteam.com.tgclouddisk.ui.fragments.AudioFragment;
import netcomteam.com.tgclouddisk.ui.fragments.DocumentsFragment;
import netcomteam.com.tgclouddisk.ui.fragments.GalleryFragment;
import netcomteam.com.tgclouddisk.ui.fragments.MyDriveFragment;
import netcomteam.com.tgclouddisk.ui.fragments.VideoFragment;
import netcomteam.com.tgclouddisk.user.User;
import netcomteam.com.tgclouddisk.user.UserInformation;
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.main_drawer)
    NavigationView mDrawer;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private int mSelectedId;
    FragmentTransaction fragmentTransaction;
    @BindView(R.id.avi)
    AVLoadingIndicatorView avi;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocaleLanguage localeLanguage = new LocaleLanguage(MainActivity.this);
        localeLanguage.loadLanguage();
        user = new User(MainActivity.this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setToolbar();
        initView();
        drawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        mDrawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();
        getInformationAboutUser();
    }

    private void setToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
    }

    private void initView() {
        mDrawer.setNavigationItemSelectedListener(this);
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onBackPressed();
                        }
                    });
                } else {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                    drawerToggle.syncState();
                    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDrawerLayout.openDrawer(GravityCompat.START);
                        }
                    });
                }
            }
        });
    }

    private void itemSelection(int mSelectedId) {
        switch (mSelectedId) {
            case R.id.my_drive:
                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_container, new MyDriveFragment());
                fragmentTransaction.commit();
                getSupportActionBar().setTitle(R.string.my_drive);
                break;
            case R.id.gallery:
                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_container, new GalleryFragment());
                fragmentTransaction.commit();
                getSupportActionBar().setTitle(R.string.gallery);
                break;
            case R.id.music:
                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_container, new AudioFragment());
                fragmentTransaction.commit();
                getSupportActionBar().setTitle(R.string.music);
                break;
            case R.id.video:
                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_container, new VideoFragment());
                fragmentTransaction.commit();
                getSupportActionBar().setTitle(R.string.video);
                break;
            case R.id.documents:
                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_container, new DocumentsFragment());
                fragmentTransaction.commit();
                getSupportActionBar().setTitle(R.string.documents);
                break;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.w("listItem", String.valueOf(data.getStringExtra("lang")));
        LocaleLanguage localeLanguage = new LocaleLanguage(MainActivity.this);
        localeLanguage.loadLanguage();
        setContentView(R.layout.activity_main);
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        item.setChecked(true);
        mSelectedId = item.getItemId();
        itemSelection(mSelectedId);
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putInt("SELECTED_ID", mSelectedId);
    }

    void getInformationAboutUser() {
        UserInfo userInfo = new UserInfo();
        userInfo.execute();
    }

    public void transactionToMydriveFragment() {
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        mDrawerLayout.closeDrawer(GravityCompat.START);
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_container, new MyDriveFragment());
        fragmentTransaction.commit();
        getSupportActionBar().setTitle(R.string.my_drive);
    }

    private class UserInfo extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            user.setGetInformationAboutUser(false);
            user.getInformationAboutUser();
            while (!user.isGetInformationAboutUser()) {
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            avi.hide();
            View headerView = mDrawer.getHeaderView(0);
            TextView drawer_header_name_user = (TextView) headerView.findViewById(R.id.drawer_header_name_user);
            drawer_header_name_user.setText(new UserInformation().getFirstName() + " " + new UserInformation().getLastName());
            ImageButton imageButton = (ImageButton) headerView.findViewById(R.id.drawer_header_exit);
            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Dialog dialog = new Dialog(MainActivity.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.settings);
                    dialog.setTitle("Choose file!!!");
                    dialog.show();
                    LinearLayout changeLang = (LinearLayout) dialog.findViewById(R.id.lang);
                    LinearLayout logout = (LinearLayout) dialog.findViewById(R.id.logout);
                    TextView textView = (TextView) dialog.findViewById(R.id.nameLang);
                    Locale locale = getResources().getConfiguration().locale;
                    textView.setText(locale.getDisplayLanguage());
                    changeLang.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(MainActivity.this, LanguagesListActivity.class);
                            startActivityForResult(intent, RESULT_FIRST_USER);
                        }
                    });
                    logout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            user.log_out(mDrawer);
                        }
                    });
                }
            });
            transactionToMydriveFragment();
        }
    }
}