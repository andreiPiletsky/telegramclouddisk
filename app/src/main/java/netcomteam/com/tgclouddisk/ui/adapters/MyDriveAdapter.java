package netcomteam.com.tgclouddisk.ui.adapters;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.geqian.progressbar.CircleProgressBar;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.picasso.transformations.BlurTransformation;
import netcomteam.com.tgclouddisk.R;
import netcomteam.com.tgclouddisk.pojo.FileModel;
;
/**
 * Created by JUNED on 6/16/2016.
 */
public class MyDriveAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater inflater;
    Context context;
    List<FileModel> data = new ArrayList<>();

    public MyDriveAdapter(Context context, List<FileModel> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View v;
        v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_file, parent, false);
        viewHolder = new MyDriveAdapter.MyItemHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (data.get(position).getPath() != null) {
            if ((!data.get(position).getPath().equals(""))) {
                ((MyItemHolder) holder).isMemoryFile.setVisibility(View.VISIBLE);
            }
        }
        if (data.get(position).getFolder() != null) {
            show_folder(holder, position);
        } else if (data.get(position).getImage() != null) {
            show_image(holder, position);
        } else if (data.get(position).getAudio() != null) {
            show_music(holder, position);
        } else if (data.get(position).getVideo() != null) {
            show_video(holder, position);
        } else if (data.get(position).getDocument() != null) {
            show_document(holder, position);
        }
    }

    void showProgress(RecyclerView.ViewHolder holder, final int position) {
        ((MyItemHolder) holder).progressBar.setVisibility(View.VISIBLE);
        int percent = (int) (((float) data.get(position).getReady() / (float) data.get(position).getSize()) * 100);
        if ((percent == 100) || data.get(position).getSize() == 0) {
            ((MyItemHolder) holder).progressBar.setVisibility(View.INVISIBLE);
        } else {
            ((MyItemHolder) holder).progressBar.setProgress(percent);
        }
    }

    void show_music(RecyclerView.ViewHolder holder, final int position) {
        Picasso picasso = Picasso.with(context);
        picasso
                .load(R.drawable.music)
                .resize(200, 200)
                .centerCrop()
                .into(((MyDriveAdapter.MyItemHolder) holder).mImg);
        showProgress(holder, position);
        ((MyItemHolder) holder).autofitTextView.setText(data.get(position).getAudio().getFileName());
    }

    void show_video(RecyclerView.ViewHolder holder, final int position) {
        Picasso picasso = Picasso.with(context);
        try {
            picasso
                    .load(new File(data.get(position).getVideo().getPhotoPath()))
                    .resize(200, 200)
                    .centerCrop()
                    .transform(new BlurTransformation(context, 15, 1))
                    .into(((MyDriveAdapter.MyItemHolder) holder).mImg);
            showProgress(holder, position);
            ((MyItemHolder) holder).autofitTextView.setText(data.get(position).getFile_id() + "." + data.get(position).getVideo().getMimeType());
        } catch (Exception e) {
        }
    }

    void show_document(RecyclerView.ViewHolder holder, final int position) {
        if (data.get(position).getDocument().getMimeType() != null) {
            if (data.get(position).getDocument().getMimeType().contains("rar")) {
                Picasso picasso = Picasso.with(context);
                picasso
                        .load(R.drawable.rar)
                        .resize(200, 200)
                        .centerCrop()
                        .into(((MyDriveAdapter.MyItemHolder) holder).mImg);
                showProgress(holder, position);
                ((MyItemHolder) holder).autofitTextView.setText(data.get(position).getDocument().getFileName());
            } else if (data.get(position).getDocument().getMimeType().contains("wordprocessingml.document")) {
                Picasso picasso = Picasso.with(context);
                picasso
                        .load(R.drawable.docx)
                        .resize(200, 200)
                        .centerCrop()
                        .into(((MyDriveAdapter.MyItemHolder) holder).mImg);
                showProgress(holder, position);
                ((MyItemHolder) holder).autofitTextView.setText(data.get(position).getDocument().getFileName());
            } else if (data.get(position).getDocument().getMimeType().contains("vnd.ms-excel")) {
                Picasso picasso = Picasso.with(context);
                picasso
                        .load(R.drawable.excel)
                        .resize(200, 200)
                        .centerCrop()
                        .into(((MyDriveAdapter.MyItemHolder) holder).mImg);
                showProgress(holder, position);
                ((MyItemHolder) holder).autofitTextView.setText(data.get(position).getDocument().getFileName());
            } else if (data.get(position).getDocument().getMimeType().contains("presentationml.presentation")) {
                Picasso picasso = Picasso.with(context);
                picasso
                        .load(R.drawable.ppt)
                        .resize(200, 200)
                        .centerCrop()
                        .into(((MyDriveAdapter.MyItemHolder) holder).mImg);
                showProgress(holder, position);
                ((MyItemHolder) holder).autofitTextView.setText(data.get(position).getDocument().getFileName());
            } else {
                Picasso picasso = Picasso.with(context);
                picasso
                        .load(R.drawable.file)
                        .resize(200, 200)
                        .centerCrop()
                        .into(((MyDriveAdapter.MyItemHolder) holder).mImg);
                showProgress(holder, position);
                ((MyItemHolder) holder).autofitTextView.setText(data.get(position).getDocument().getFileName());
            }
        } else {
            Picasso picasso = Picasso.with(context);
            picasso
                    .load(R.drawable.file)
                    .resize(200, 200)
                    .centerCrop()
                    .into(((MyDriveAdapter.MyItemHolder) holder).mImg);
            showProgress(holder, position);
            ((MyItemHolder) holder).autofitTextView.setText(data.get(position).getDocument().getFileName());
        }
    }

    void show_folder(RecyclerView.ViewHolder holder, final int position) {
        Picasso picasso = Picasso.with(context);
        picasso
                .load(R.drawable.folder)
                .resize(200, 200)
                .centerCrop()
                .into(((MyDriveAdapter.MyItemHolder) holder).mImg);
        ((MyItemHolder) holder).autofitTextView.setText(data.get(position).getFolder().getFolderName());
    }

    void show_image(RecyclerView.ViewHolder holder, final int position) {
        Picasso picasso = Picasso.with(context);
        if (data.get(position).getPath() == null) {
            picasso
                    .load(R.drawable.img)
                    .resize(200, 200)
                    .centerCrop()
                    .into(((MyDriveAdapter.MyItemHolder) holder).mImg);
            showProgress(holder, position);
            ((MyItemHolder) holder).autofitTextView.setText(data.get(position).getImage().getPhoto_id() + ".jpg");
        } else {
            picasso
                    .load(new File(data.get(position).getPath()))
                    .resize(200, 200)
                    .centerCrop()
                    .into(((MyDriveAdapter.MyItemHolder) holder).mImg);
            showProgress(holder, position);
            ((MyItemHolder) holder).autofitTextView.setText(data.get(position).getImage().getPhoto_id() + ".jpg");
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyItemHolder extends RecyclerView.ViewHolder {

        ImageView mImg, isMemoryFile;
        TextView autofitTextView;
        CircleProgressBar progressBar;

        public MyItemHolder(View itemView) {
            super(itemView);
            progressBar = (CircleProgressBar) itemView.findViewById(R.id.circleProgressBar);
            mImg = (ImageView) itemView.findViewById(R.id.image_view);
            autofitTextView = (TextView) itemView.findViewById(R.id.fileName);
            isMemoryFile = (ImageView) itemView.findViewById(R.id.is_memory_file);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    int dpToPx() {
        int dp = (int) context.getResources().getDimension(R.dimen.height_image);
        float density = context.getResources().getDisplayMetrics().density;
        float px = (dp * density);
        return (int) px;
    }
}
