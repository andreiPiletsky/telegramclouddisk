package netcomteam.com.tgclouddisk.ui.fragments;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.apache.commons.lang3.StringUtils;
import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import netcomteam.com.tgclouddisk.Const;
import netcomteam.com.tgclouddisk.R;
import netcomteam.com.tgclouddisk.handlers.ApiHelper;
import netcomteam.com.tgclouddisk.pojo.ChatModel;
import netcomteam.com.tgclouddisk.pojo.FileModel;
import netcomteam.com.tgclouddisk.pojo.FolderModel;
import netcomteam.com.tgclouddisk.ui.adapters.MyDriveAdapter;
/**
 * Created by andreika on 19.10.17.
 */
public class FolderFragment extends BaseFragment {

    MyDriveAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getBundle();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    void initAdapter() {
        mAdapter = new MyDriveAdapter(getActivity(), data);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    void updateAdapter() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        initAppBar();
    }

    void initAppBar() {
        final AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        activity.getSupportActionBar().setTitle(nameFragment);
    }

    @Override
    void getFolder() {
        Log.w("folderrrrrrrrrrrrrrrrrrrrrrrr", String.valueOf(levelOfDirectory));
        isHasFolder = false;
        TdApi.SearchChatMessages searchChatMessages = new TdApi.SearchChatMessages(new ChatModel().getId(),
                "FolderList", 0, Const.COUNT_LOAD_MESSAGES, new TdApi.SearchMessagesFilterEmpty());
        TG.getClientInstance().send(searchChatMessages, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                TdApi.Messages messages = (TdApi.Messages) object;
                TdApi.Message message = (TdApi.Message) messages.messages[0];
                folder_message_id = message.id;
                if (TdApi.MessageText.CONSTRUCTOR == ((TdApi.MessageText) message.content).getConstructor()) {
                    TdApi.MessageText messageText = (TdApi.MessageText) message.content;
                    listFolder = messageText.text.split(System.getProperty("line.separator"));
                    for (int i = 1; i < listFolder.length; i++) {
                        int startPos = StringUtils.ordinalIndexOf(listFolder[i], "/", levelOfDirectory);
                        int endPos = StringUtils.ordinalIndexOf(listFolder[i], "/", levelOfDirectory + 1);
                        if ((startPos > -1) && (endPos > -1)) {
                            if (listFolder[i].substring(0, startPos + 1).equals(pathFolder)) {
                                folderList.add(listFolder[i].substring(0, endPos + 1));
                            }
                        }
                    }
                    new ApiHelper().deleteRepeatFolder(folderList);
                    for (int i = 0; i < folderList.size(); i++) {
                        int startPos = StringUtils.ordinalIndexOf(folderList.get(i), "/", levelOfDirectory);
                        int endPos = StringUtils.ordinalIndexOf(folderList.get(i), "/", levelOfDirectory + 1);
                        if ((startPos > -1) && (endPos > -1)) {
                            countFolder++;
                            FileModel fileModel = new FileModel();
                            FolderModel folderModel = new FolderModel();
                            folderModel.setPath(folderList.get(i));
                            folderModel.setFolderName(folderList.get(i).substring(startPos + 1, endPos));
                            folderModel.setMessage_id(message.id);
                            fileModel.setFolder(folderModel);
                            data.add(fileModel);
                            isGetFolders = true;
                            asynThread = new BaseFragment.AsynThread();
                            asynThread.execute();
                        }
                    }
                    isGetFolders = true;
                }
            }
        });
    }

    @Override
    void getFiles() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                while (!isGetFolders) {
                }
                Log.w("pathfolder", pathFolder);
                TdApi.SearchChatMessages searchChatMessages = new TdApi.SearchChatMessages(new ChatModel().getId(),
                        pathFolder, last_messages, Const.COUNT_LOAD_MESSAGES, new TdApi.SearchMessagesFilterEmpty());
                TG.getClientInstance().send(searchChatMessages, new Client.ResultHandler() {
                    @Override
                    public void onResult(TdApi.TLObject object) {
                        TdApi.Messages messages = (TdApi.Messages) object;
                        for (int i = 0; i < messages.messages.length; i++) {
                            last_messages = messages.messages[i].id;
                            TdApi.Message message = (TdApi.Message) messages.messages[i];
                            Log.w("messagessssssssssss", String.valueOf(message));
                            if (message.content.getConstructor() == TdApi.MessagePhoto.CONSTRUCTOR) {
                                TdApi.MessagePhoto photo = (TdApi.MessagePhoto) message.content;
                                if (photo.caption.equals(pathFolder)) {
                                    FileModel fileModel = new ApiHelper().getImage(message, photo);
                                    data.add(fileModel);
                                    TdApi.DownloadFile downloadFile = new TdApi.DownloadFile((fileModel.getFile_id()));
                                    TG.getClientInstance().send(downloadFile, new Client.ResultHandler() {
                                        @Override
                                        public void onResult(TdApi.TLObject object) {
                                        }
                                    });
                                }
                            } else if (message.content.getConstructor() == TdApi.MessageAudio.CONSTRUCTOR) {
                                TdApi.MessageAudio audio = (TdApi.MessageAudio) message.content;
                                if (audio.caption.equals(pathFolder)) {
                                    FileModel fileModel = new ApiHelper().getAudio(message, audio);
                                    data.add(fileModel);
//                                    asynThread = new BaseFragment.AsynThread();
//                                    asynThread.execute();
                                }
                            } else if (message.content.getConstructor() == TdApi.MessageVideo.CONSTRUCTOR) {
                                TdApi.MessageVideo video = (TdApi.MessageVideo) message.content;
                                FileModel fileModel = new ApiHelper().getVideo(message, video);
                                data.add(fileModel);
                                TdApi.DownloadFile downloadFile = new TdApi.DownloadFile((fileModel.getVideo().getPhoto_id()));
                                TG.getClientInstance().send(downloadFile, new Client.ResultHandler() {
                                    @Override
                                    public void onResult(TdApi.TLObject object) {
                                    }
                                });
//                                asynThread = new BaseFragment.AsynThread();
//                                asynThread.execute();
                            } else if (message.content.getConstructor() == TdApi.MessageDocument.CONSTRUCTOR) {
                                TdApi.MessageDocument document = (TdApi.MessageDocument) message.content;
                                FileModel fileModel = new ApiHelper().getDocument(message, document);
                                data.add(fileModel);
//                                asynThread = new BaseFragment.AsynThread();
//                                asynThread.execute();
                            }
                        }
                        asynThread = new BaseFragment.AsynThread();
                        asynThread.execute();
                    }
                });
                loading = true;
                return null;
            }
        }.execute();
    }

    @Override
    void fragmentTransaction(String path, String name) {
        super.fragmentTransaction(path, name);
    }

    void getBundle() {
        Bundle arguments = getArguments();
        pathFolder = arguments.getString("pathFolder");
        nameFragment = arguments.getString("folderName");
        levelOfDirectory = arguments.getInt("levelFolder");
        Log.w("name", String.valueOf(levelOfDirectory));
    }
}