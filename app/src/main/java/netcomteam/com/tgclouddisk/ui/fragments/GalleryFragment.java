package netcomteam.com.tgclouddisk.ui.fragments;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import netcomteam.com.tgclouddisk.Const;
import netcomteam.com.tgclouddisk.R;
import netcomteam.com.tgclouddisk.handlers.ApiHelper;
import netcomteam.com.tgclouddisk.pojo.ChatModel;
import netcomteam.com.tgclouddisk.pojo.FileModel;
import netcomteam.com.tgclouddisk.ui.activity.FullScreenImageActivity;
import netcomteam.com.tgclouddisk.ui.adapters.GalleryAdapter;
import netcomteam.com.tgclouddisk.ui.adapters.RecyclerItemClickListener;
/**
 * A simple {@link Fragment} subclass.
 */
public class GalleryFragment extends BaseFragment {

    GalleryAdapter mAdapter;

    @Override
    void initAdapter() {
        mAdapter = new GalleryAdapter(getActivity(), data);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    void initGUI(View view) {
        gridLayoutManager = new GridLayoutManager(getActivity(), 4);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.list);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        Button button = (Button) view.findViewById(R.id.sendFileToCloud);
        button.setVisibility(View.INVISIBLE);
    }

    @Override
    public void loadData() {
        last_messages = 0;
        Log.w("sdsdfs0", "sdfsdf");
        data.clear();
        getFiles();
    }

    @Override
    void getFiles() {
        TdApi.SearchChatMessages searchChatMessages = new TdApi.SearchChatMessages(new ChatModel().getId(),
                "", last_messages, Const.COUNT_LOAD_MESSAGES, new TdApi.SearchMessagesFilterPhoto());
        TG.getClientInstance().send(searchChatMessages, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                Log.w("messagessssssssssss", String.valueOf(object));
                TdApi.Messages messages = (TdApi.Messages) object;
                for (int i = 0; i < messages.messages.length; i++) {
                    last_messages = messages.messages[i].id;
                    TdApi.Message message = (TdApi.Message) messages.messages[i];
                    if (message.content.getConstructor() == TdApi.MessagePhoto.CONSTRUCTOR) {
                        TdApi.MessagePhoto photo = (TdApi.MessagePhoto) message.content;
                        FileModel fileModel = new ApiHelper().getImage(message, photo);
                        data.add(fileModel);
                        Log.w("phot0", String.valueOf(photo.photo.id));
                        TdApi.DownloadFile downloadFile = new TdApi.DownloadFile((fileModel.getFile_id()));
                        TG.getClientInstance().send(downloadFile, new Client.ResultHandler() {
                            @Override
                            public void onResult(TdApi.TLObject object) {
                            }
                        });
                    }
                }
                asynThread = new BaseFragment.AsynThread();
                asynThread.execute();
            }
        });
        loading = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        initAppBar();
    }

    void initAppBar() {
        final AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_side_bar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setHomeButtonEnabled(true);
        activity.getSupportActionBar().setTitle(R.string.gallery);
    }

    @Override
    void updateAdapter() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    void adapterClickHandler() {
        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if (isSelectionFiles) {
                            final ImageView tickImage = (ImageView) view.findViewById(R.id.tick_image);
                            final ImageView mImg = (ImageView) view.findViewById(R.id.image_view);
                            if (!tickImage.isShown()) {
                                attachFiles.add(position);
                                tickImage.setVisibility(View.VISIBLE);
                            } else {
                                for (int i = 0; i < attachFiles.size(); i++) {
                                    if (attachFiles.get(i) == position) {
                                        attachFiles.remove(i);
                                    }
                                }
                                tickImage.setVisibility(View.INVISIBLE);
                            }
                        } else {
                            if (data.get(position).getImage() != null) {
                                Intent intent = new Intent(getContext(), FullScreenImageActivity.class);
                                intent.putExtra("url", data.get(position).getPath());
//                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);

                            }
                        }
                    }
                })
        );
    }
}




