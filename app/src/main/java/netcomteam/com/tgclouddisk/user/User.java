package netcomteam.com.tgclouddisk.user;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import netcomteam.com.tgclouddisk.R;
import netcomteam.com.tgclouddisk.handlers.ApiHelper;
import netcomteam.com.tgclouddisk.pojo.ChatModel;
import netcomteam.com.tgclouddisk.ui.activity.ActivationActivity;
import netcomteam.com.tgclouddisk.ui.activity.AutorizationActivity;
import netcomteam.com.tgclouddisk.ui.activity.CountriesListActivity;
/**
 * Created by andreika on 27.11.17.
 */
public class User {

    Context mContext;
    private boolean isGetInformationAboutUser;

    public boolean isGetInformationAboutUser() {
        return isGetInformationAboutUser;
    }

    public void setGetInformationAboutUser(boolean getInformationAboutUser) {
        isGetInformationAboutUser = getInformationAboutUser;
    }

    public User(Context context) {
        this.mContext = context;
    }

    public void log_out(NavigationView mDrawer) {
        TdApi.ResetAuth resetAuth = new TdApi.ResetAuth();
        TG.getClientInstance().send(resetAuth, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                Intent intent = new Intent(mContext, AutorizationActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
                ((Activity) mContext).finish();
            }
        });



    }

    public void log_in(final String phoneNumber, final TextView country_code, final EditText input_phoneNumber) {
        TdApi.SetAuthPhoneNumber setPhone = new TdApi.SetAuthPhoneNumber(phoneNumber, true, true);
        TG.getClientInstance().send(setPhone, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                Log.w("object", String.valueOf(object));
                Log.w("objectphonenumber", String.valueOf(phoneNumber));
                Intent intent = new Intent(mContext, ActivationActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("phone", country_code.getText().toString() + input_phoneNumber);
                mContext.startActivity(intent);
                ((Activity) mContext).finish();
            }
        });
    }

    public void getInformationAboutUser() {
        TdApi.GetMe getMe = new TdApi.GetMe();
        TG.getClientInstance().send(getMe, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                TdApi.User user = (TdApi.User) object;
                new UserInformation().setId(user.id);
                new UserInformation().setFirstName(user.firstName);
                new UserInformation().setLastName(user.lastName);
                new UserInformation().setPhone_number(user.phoneNumber);
                TdApi.CreatePrivateChat createPrivateChat = new TdApi.CreatePrivateChat(user.id);
                TG.getClientInstance().send(createPrivateChat, new Client.ResultHandler() {
                    @Override
                    public void onResult(TdApi.TLObject object) {
                        TdApi.Chat chat = (TdApi.Chat) object;
                        new ChatModel().setId(chat.id);
                        new ChatModel().setTopMessage(chat.topMessage);
                        new ApiHelper().CreateFolderMessage();
                        isGetInformationAboutUser = true;
                    }
                });
            }
        });
    }
}
