import android.app.Application;
import android.os.Environment;
import android.util.Log;

import org.drinkless.td.libcore.telegram.TG;

import java.io.File;

import netcomteam.com.tgclouddisk.R;
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            String path = Environment.getExternalStorageDirectory() + File.separator + "Android" + File.separator + "data" + File.separator + getPackageName();
            TG.setDir(path);
            TG.setFilesDir(Environment.getExternalStorageDirectory()+ File.separator + getResources().getString(R.string.app_name));
        }
    }
}
